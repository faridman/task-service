package az.abb.taskify.repository;

import az.abb.taskify.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer , Long> {
    Optional<Customer> findByUserName(String userName);
}
