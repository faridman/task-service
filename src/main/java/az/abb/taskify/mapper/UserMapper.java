package az.abb.taskify.mapper;

import az.abb.taskify.dto.UserDto;
import az.abb.taskify.dto.SignUpDto;
import az.abb.taskify.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "organizationEntity.organName", source = "organizationName")
    User toEntity(SignUpDto signUpDto);

    User toEntity(UserDto userDto);

}
