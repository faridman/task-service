package az.abb.taskify.mapper;

import az.abb.taskify.dto.SignUpDto;
import az.abb.taskify.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(target = "organizationEntity.organName", source = "organizationName")
    Customer toEntity(SignUpDto signUpDto);
}
