package az.abb.taskify.service;

import az.abb.taskify.dto.AssignTaskDto;
import az.abb.taskify.dto.TaskDTO;

import javax.mail.MessagingException;

public interface TaskService {
    void createTask(TaskDTO taskDTO);

    void assignTask(AssignTaskDto assignTaskDto) throws MessagingException;
}
