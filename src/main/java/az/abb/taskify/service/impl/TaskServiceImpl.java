package az.abb.taskify.service.impl;

import az.abb.taskify.dto.TaskDTO;
import az.abb.taskify.mapper.TaskMapper;
import az.abb.taskify.repository.TaskRepository;
import az.abb.taskify.dto.AssignTaskDto;
import az.abb.taskify.entity.Task;
import az.abb.taskify.entity.User;
import az.abb.taskify.entity.UserTask;
import az.abb.taskify.repository.UserRepository;
import az.abb.taskify.repository.UserTaskRepository;
import az.abb.taskify.service.SendMailService;
import az.abb.taskify.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

import java.util.Optional;

import static az.abb.taskify.enums.TaskStatus.NOT_ASSIGNED;

@Service
@Slf4j
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final SendMailService mailService;
    private final UserRepository userRepository;
    private final UserTaskRepository userTaskRepository;

    public TaskServiceImpl(TaskRepository taskRepository, SendMailService mailService, UserRepository userRepository, UserTaskRepository userTaskRepository) {
        this.taskRepository = taskRepository;
        this.mailService = mailService;
        this.userRepository = userRepository;
        this.userTaskRepository = userTaskRepository;
    }

    @Override
    public void createTask(TaskDTO taskDTO) {
        Task task = TaskMapper.INSTANCE.toEntity(taskDTO);
        task.setAssigned(NOT_ASSIGNED);
        taskRepository.save(task);
    }

    @Override
    public void assignTask(AssignTaskDto assignTaskDto) throws MessagingException {
        Optional<User> user = userRepository.findById(assignTaskDto.getUserId());
        Optional<Task> task = taskRepository.findById(assignTaskDto.getTaskId());

        if (user.isPresent() && task.isPresent()) {
            UserTask userTask = new UserTask();
            userTask.setTask(task.get());
            userTask.setUser(user.get());
            userTaskRepository.save(userTask);

            mailService.send(user.get().getEmail(), "Task Assigned", userTask.getTask().getTitle());
        }
    }


}
