package az.abb.taskify.service.impl;

import az.abb.taskify.dto.UserDto;
import az.abb.taskify.entity.Role;
import az.abb.taskify.entity.User;
import az.abb.taskify.exeption.EmailAlreadyUsedExeption;
import az.abb.taskify.mapper.UserMapper;
import az.abb.taskify.repository.RoleRepository;
import az.abb.taskify.repository.UserRepository;
import az.abb.taskify.service.UserService;

import az.abb.taskify.util.PasswordGenerator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public void createUser(UserDto userDto) {
        userRepository.findByEmail(userDto.getEmail())
                .ifPresent(
                        user -> {
                            throw new EmailAlreadyUsedExeption(user.getEmail());
                        }
                );

        Role role = roleRepository
                .findByRoleName("user")
                .orElseGet(() -> Role.builder().roleName("user").build());

        User user = UserMapper.INSTANCE.toEntity(userDto);
        user.setPassword(PasswordGenerator.generateStrongPassword());
        user.setRoleEntity(role);
        userRepository.save(user);
    }

}
