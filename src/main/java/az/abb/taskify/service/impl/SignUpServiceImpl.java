package az.abb.taskify.service.impl;

import az.abb.taskify.entity.Organization;
import az.abb.taskify.entity.Role;
import az.abb.taskify.exeption.UserNameAlreadyUsedExeption;
import az.abb.taskify.repository.OrganizationRepository;
import az.abb.taskify.repository.RoleRepository;
import az.abb.taskify.repository.UserRepository;
import az.abb.taskify.service.SignUpService;
import az.abb.taskify.dto.SignUpDto;
import az.abb.taskify.entity.Customer;
import az.abb.taskify.mapper.CustomerMapper;
import az.abb.taskify.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
//import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;
   // private final PasswordEncoder passwordEncoder;

    public SignUpServiceImpl(UserRepository userRepository, OrganizationRepository organizationRepository, RoleRepository roleRepository, CustomerRepository customerRepository) {
        this.userRepository = userRepository;
        this.organizationRepository = organizationRepository;
        this.roleRepository = roleRepository;
        this.customerRepository = customerRepository;
    //    this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public void signUp(SignUpDto signUpDTO) {
        customerRepository.findByUserName(signUpDTO.getUserName())
                .ifPresent(
                        user -> {
                    throw new UserNameAlreadyUsedExeption(signUpDTO.getUserName());
                });

        Customer customer = CustomerMapper.INSTANCE.toEntity(signUpDTO);

            //  customer.setPassword(passwordEncoder.encode(signUpDTO.getPassword()));

        Role role = roleRepository
                .findByRoleName("admin")
                .orElseGet(() -> Role.builder().roleName("admin").build());

        Organization organizationEntity = organizationRepository
                .findByOrganName(signUpDTO.getOrganizationName())
                .orElseGet(() -> Organization.builder().organName(signUpDTO.getOrganizationName()).build());

        customer.setRoleEntity(role);
        customer.setOrganizationEntity(organizationEntity);

        customerRepository.save(customer);
    }
}
