package az.abb.taskify.service;

import az.abb.taskify.dto.SignUpDto;

public interface SignUpService {

    void signUp(SignUpDto signUpDTO);

}
