package az.abb.taskify.service;


import az.abb.taskify.dto.UserDto;

public interface UserService {

    void createUser(UserDto userDto);

}
