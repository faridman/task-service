package az.abb.taskify.config;

public final class PasswordConfig {

    public static final int PASSWORD_MIN_LENGTH = 6;

    public PasswordConfig() {
    }

}
