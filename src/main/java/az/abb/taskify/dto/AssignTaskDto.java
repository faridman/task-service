package az.abb.taskify.dto;

import lombok.Data;

@Data
public class AssignTaskDto {

    private long userId;
    private long taskId;

}
