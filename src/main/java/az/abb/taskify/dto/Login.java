package az.abb.taskify.dto;

import az.abb.taskify.config.PasswordConfig;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class Login {

    @NotNull
    @Size(min = 1, max = 50)
    private String email;

    @NotNull
    @Size(min = PasswordConfig.PASSWORD_MIN_LENGTH)
    private String password;

}
