package az.abb.taskify.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpDto {

    private String userName;
    private String organizationName;
    private String address;
    private String phoneNumber;
    private String email;
    private String password;

}
