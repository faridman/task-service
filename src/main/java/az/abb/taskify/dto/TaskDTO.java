package az.abb.taskify.dto;

import lombok.Data;

@Data
public class TaskDTO {

    private String title;
    private String description;
    private String deadLine;
    private String status;

}
