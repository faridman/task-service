package az.abb.taskify.enums;

public enum TaskStatus {
    NOT_ASSIGNED,
    ASSIGNED
}
