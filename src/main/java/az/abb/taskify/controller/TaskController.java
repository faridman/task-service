package az.abb.taskify.controller;

import az.abb.taskify.dto.AssignTaskDto;
import az.abb.taskify.dto.TaskDTO;
import az.abb.taskify.service.TaskService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequestMapping(value = "/task")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create")
    public void createTask(@RequestBody TaskDTO taskDTO) {
        taskService.createTask(taskDTO);
    }

    @PostMapping(value = "/assign")
    public void assignTask(@RequestBody AssignTaskDto assignTaskDto) throws MessagingException {
        taskService.assignTask(assignTaskDto);
    }

}
