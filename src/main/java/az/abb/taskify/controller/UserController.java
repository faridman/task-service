package az.abb.taskify.controller;

import az.abb.taskify.dto.UserDto;
import az.abb.taskify.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/create")
    public void createUser(@RequestBody UserDto userDto) {
        userService.createUser(userDto);
    }
}
