package az.abb.taskify.controller;

import az.abb.taskify.service.SignUpService;
import az.abb.taskify.dto.SignUpDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/signUp")
public class SignUpController {

    private SignUpService signUpService;

    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @PostMapping
    public void signUp(@RequestBody SignUpDto signUpDTO) {
        signUpService.signUp(signUpDTO);
    }

}
