package az.abb.taskify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Etaskify {

    public static void main(String[] args) {
        SpringApplication.run(Etaskify.class, args);
    }

}
