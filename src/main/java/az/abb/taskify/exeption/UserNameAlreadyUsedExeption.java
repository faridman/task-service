package az.abb.taskify.exeption;

public class UserNameAlreadyUsedExeption extends RuntimeException {

    public static final String USERNAME_ALREADY_USED = "Username \"%s\" already registered";
    private static final long serialVersionUID = 1L;

    public UserNameAlreadyUsedExeption(String userName) {
        super(String.format(USERNAME_ALREADY_USED, userName));
    }
}
