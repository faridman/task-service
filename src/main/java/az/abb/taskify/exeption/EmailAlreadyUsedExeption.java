package az.abb.taskify.exeption;

public class EmailAlreadyUsedExeption extends RuntimeException {
    public static final String EMAIL_ALREADY_USED = "Email \"%s\" already registered";
    private static final long serialVersionUID = 1L;

    public EmailAlreadyUsedExeption(String email) {
        super(String.format(EMAIL_ALREADY_USED, email));
    }

}
