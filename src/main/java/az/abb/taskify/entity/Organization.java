package az.abb.taskify.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "organizations")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String organName;

}
