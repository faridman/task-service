package az.abb.taskify.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class TaskAssign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long task_id;
    private long user_id;

}
