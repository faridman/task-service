package az.abb.taskify.entity;

import az.abb.taskify.enums.TaskStatus;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "tasks")
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String description;
    private String deadLine;

    @Enumerated(EnumType.ORDINAL)
    private TaskStatus assigned;
    private String status;

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserTask> userTasks = new ArrayList<>();
}
